// Soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"]
daftarHewan.sort()
daftarHewan.forEach(myFunction);

function myFunction(value) {
    return value + "<br>"; 
}
console.log(myFunction(daftarHewan))

// Soal 2
function introduce({name,age,address,hobby}){
    return "Nama saya " + name + ",umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 

// Soal 3
const vokal = ["a", "e", "i", "o", "u"]
function hitung_huruf_vokal(nama){
    // initialize count
    let count = 0;

    // loop through string to test if each character is a vowel
    for (let huruf of nama.toLowerCase()) {
        if (vokal.includes(huruf)) {
            count++;
        }
    }

    // return number of vokal
    return count
}
var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2

// Soal 4

function hitung(angka){
    return angka * 2 - 2 
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8