<?php

namespace App;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable =['name'];
    protected $primarykey ='id';
    protected $keyType ='string';
    public $incrementing = false;

    public function user()
    {
        return $this->hasMany('App\User');
    }

    protected static function boot(){
        parent::boot();
        static::creating( function($model){
            if ( empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }
}


?>
