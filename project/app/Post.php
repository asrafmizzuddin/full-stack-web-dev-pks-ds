<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable =['title','description','user_id'];
    protected $primarykey ='id';
    protected $keyType ='string';
    public $incrementing = false;

    public function comment(){
        return $this->hasMany('App\comments');
    }

    protected static function boot(){
        parent::boot();
        static::creating( function($model){
            if ( empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid();
            }
            // $model->user_id = auth()->user()->id;
        });
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
