<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\OtpCode;
use App\User;
use Carbon\Carbon;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // dd('masuk');
         //set validation
         $validator = Validator::make($request->all(), [
            'name'   => 'required',
            'email' => 'required | unique:users,email|email',
            'username' => 'required | unique:users,username'
        ]);
        // dd($request->all());
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $user = User::create($request->all());

        do {
            $random = mt_rand(100000 , 999999);
            $check = OtpCode::where('otp',$random)->first();
        } while ($check);

        $now = Carbon::now();
        $otp_code = OtpCode::create([
            'otp' => $random,
            'valid_until' => $now->addMinutes(5),
            'user_id' => $user->id
        ]);
        // kirim email
        // 

        return response()->json([
            'success' => true,
            'message' => 'Data user berhasil dibuat',
            'data' => [
                'user' => $user,
                'otp_code' => $otp_code
            ]
        ]);
    }
}
