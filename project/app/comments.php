<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class comments extends Model
{
    protected $fillable =['content','user_id','post_id'];
    protected $primarykey ='id';
    protected $keyType ='string';
    public $incrementing = false;

    public function post(){
        return $this->belongsTo('App\Post');
    }

    protected static function boot(){
        parent::boot();
        static::creating( function($model){
            if ( empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
