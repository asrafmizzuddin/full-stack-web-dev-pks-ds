<?php


use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
/**
 * route resource post
 */
Route::apiResource('/post', 'postsController');
Route::apiResource('/role', 'rolesController');
Route::apiResource('/comment', 'commentsController');

Route::group([
    'prefix' => 'auth',
    'namespace' => 'Auth'
], function(){
    Route::post('register', 'RegisterController')->name('authRegister');

    Route::post('regenerate-otp-code', 'RegenerateOtpCodeController')->name('authRegenerate');

    Route::post('verification', 'VerificationController')->name('authVerification');

    Route::post('update-password', 'UpdatePasswordController')->name('authUpdatePassword');

    Route::post('login', 'LoginController')->name('authLogin');

});