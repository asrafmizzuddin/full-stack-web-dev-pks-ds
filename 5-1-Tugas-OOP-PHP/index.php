<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Index</title>
</head>
<body>
<?php

trait Fight {
    public $attackPower;
    public $defensePower;

    public function serang($lawan) {
        if ($lawan->darah <= 0) {
            echo $lawan->nama . " sudah kalah <br>";

        } else {
            echo $this->nama . " sedang menyerang " . $lawan->nama . "<br>";
            return $lawan->diserang($this);
        }
    }

    public function diserang($penyerang) {
        echo $this->nama . " sedang diserang" . "<br>";
        $this->darah -= ($penyerang->attackPower/$this->defensePower);

        echo "Darah dari " . $this->nama . " berkurang menjadi " . $this->darah . "<br>";

        if ($this->darah <= 0) {
            echo $this->nama . " telah kalah! <br>";
        }

    }
}

abstract class Hewan {
    use Fight;
    public $nama;
    public $jumlahKaki;
    public $keahlian = "";
    public $darah = 50;
    public function __construct($nama) {
        $this->nama = $nama;
    }

    public function atraksi() {
        echo $this->nama . " sedang " . $this->keahlian . "<br>";
    }
}

class Elang extends Hewan {
    public $jumlahKaki = 2;
    public $keahlian = "terbang tinggi";
    public $attackPower = 10;
    public $defensePower = 5;

    public function getInfoHewan() {
        echo "Nama Hewan: " . $this->nama . "<br> Jenis Hewan: " . get_class($this) 
        . "<br> Jumlah kaki: " . $this->jumlahKaki . "<br> Keahlian: " . $this->keahlian 
        . "<br> Sisa darah: " . $this->darah . "<br> Attack Power: " . $this->attackPower . "<br> Defense Power: " . $this->defensePower . "<br>";
    }
}

class Harimau extends Hewan {
    public $jumlahKaki = 4;
    public $keahlian = "Lari cepat";
    public $attackPower = 7;
    public $defensePower = 8;

    public function getInfoHewan() {
        echo "Nama Hewan: " . $this->nama . "<br> Jenis Hewan: " . get_class($this) 
        . "<br> Jumlah kaki: " . $this->jumlahKaki . "<br> Keahlian: " . $this->keahlian 
        . "<br> Sisa darah: " . $this->darah . "<br> Attack Power: " . $this->attackPower . "<br> Defense Power: " . $this->defensePower . "<br>";
    }
}

// Test Drive
echo "<h3>Animal Fighter</h3>";

$elang = new Elang("Gold Griffin");
$harimau = new Harimau("White Tiger");

echo $elang->nama . "<br>"; 
echo $elang->jumlahKaki . "<br>"; 
echo $elang->keahlian . "<br> <br>"; 

echo $harimau->nama . "<br>"; 
echo $harimau->jumlahKaki . "<br>"; 
echo $harimau->keahlian . "<br> <br>"; 

$elang->atraksi();
echo "<br>";
$elang->getInfoHewan();
echo "<br>";
$harimau->atraksi();
echo "<br>";
$elang->serang($harimau);
$harimau->serang($elang);
$elang->serang($harimau);
$harimau->serang($elang);
$elang->serang($harimau);
echo "<br>";
$elang->getInfoHewan();

?>

</body>

</html>